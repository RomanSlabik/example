<?php

namespace common\models\yii2mod\cashier;

use Dompdf\Exception;
use Yii;
use Stripe\Plan;
use Stripe\Stripe;
/**
 * This is the model class for table "subscription_plan".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $count
 *
 * @property Subscription[] $subscriptions
 */
class SubscriptionPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_api_id'], 'required'],
            [['plan_api_id'], 'unique'],
            [['id', 'number_employee', 'number_employee'], 'integer'],
            [['subscription_quantity'], 'double'],
            [['plan_api_id', 'nickname', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_api_id' => 'Stripe Plan id',
            'name' => 'Name',
            'price' => 'Price',
            'count' => 'Count',
        ];
    }

    /**
     * Get stripe Subscriptions
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['stripe_plan' => 'id']);
    }

    /**
     * Create subscription plan
     * @param array $data
     * @return bool
     */
    public function createStripePlan($data)
    {
        try {
            Stripe::setApiKey(Yii::$app->params['stripe']['apiKey']);
            Plan::create(array(
                "amount" =>  (int)$data["price"],
                "interval" => "month",
                "product" => array(
                    "name" => $data["name"]
                ),
                "currency" => "usd",
                "id" => $data["name"]
            ));
        }catch (Exception $exception){
            Yii::error($exception->getMessage()." ".$exception->getTraceAsString());
            return false;
        }
        return true;
    }

    public function isPlanExist($name)
    {
       try {
           Stripe::setApiKey(Yii::$app->params['stripe']['apiKey']);
           return Plan::retrieve($name)->active;
       }catch (Exception $exception){
           Yii::error($exception->getMessage()." ".$exception->getTraceAsString());
            return false;
       }
    }
}
