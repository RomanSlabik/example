<?php

namespace backend\controllers;

use common\models\User;
use Exception;
use Stripe\Event as StripeEvent;
use Yii;
use yii\helpers\Inflector;
use yii\web\Response;
use common\models\yii2mod\cashier\models\SubscriptionModel;
use common\models\yii2mod\cashier\SubscriptionPlan;

/**
 * Class WebhookController
 *
 * @package yii2mod\cashier\controllers
 */
class WebhookController extends BaseController
{
    /**
     * @var bool whether to enable CSRF validation for the actions in this controller
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */

    /**
     * Processing Stripe's callback and making membership updates according to payment data
     *
     * @return void|Response
     */

    public function actionHandleWebhook()
    {
        $payload = json_decode(Yii::$app->request->getRawBody(), true);


        $method = 'handle' . Inflector::camelize(str_replace('.', '_', $payload['type']));

        if (method_exists($this, $method)) {
            return $this->{$method}($payload);
        }
        return $this->missingMethod();
    }

    /**
     * Handle Stripe subscription plan.
     * @param array $data
     * @return Response
     */
    protected function handlePlan($data)
    {
        $data = $data['data']['object'];
        $nickname = !empty($data['nickname']) ? $data['nickname'] : null;
        $plan_id = str_replace('_00000000000000', '', $data['id']);
        $numberEmployees = !empty($data['metadata']['number employees']) ? $data['metadata']['number employees'] : null;
        $subscriptionQuantity = !empty($data['metadata']['subscription quantity']) ? $data['metadata']['subscription quantity'] : null;
        $type = !empty($data['metadata']['type']) ? $data['metadata']['type'] : null;
        $newData = [
            'plan_api_id' => $plan_id,
            'nickname' => ($nickname),
            'number_employee' => (int)(trim($numberEmployees)),
            'subscription_quantity' => (float)trim($subscriptionQuantity),
            'type' => strtolower(trim($type))
        ];
        return $newData;
    }

    /**
     * Handle Stripe subscription plan update.
     * @param array $data
     * @return Response
     */
    protected function handlePlanUpdated($data)
    {
        $data = $this->handlePlan($data);
        $model = SubscriptionPlan::find()->where(["plan_api_id" => $data['plan_api_id']])->one();
        try {
            if ($model->load($data, '')) {
                $model->update();
            }
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
        }

    }

    /**
     * Handle Stripe subscription plan create
     * @param array $data
     * @return Response
     */

    protected function handlePlanCreated($data)
    {
        $data = $this->handlePlan($data);
        $model = SubscriptionPlan::find()->where(["plan_api_id" => $data['plan_api_id']])->one();
        try {
            if (!$model) {
                $model = new SubscriptionPlan();
                if ($model->load($data, '')) {
                    $model->save();
                }
            }
            if ($model->load($data, '')) {
                $model->update();
            }
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
        }

    }

    /**
     * Handle Stripe subscription plan delete
     * @param array $data
     * @return Response
     */
    protected function handlePlanDeleted($data)
    {
        $data = $data['data']['object'];
        $plan_id = str_replace('_00000000000000', '', $data['id']);
        $model = SubscriptionPlan::find()->where(["plan_api_id" => $plan_id])->one();
        try {
            if ($model) {
                SubscriptionModel::deleteAll(['stripe_plan' => $model->id]);
                $model->delete();
            }
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
        }
    }

    /**
     * Handle Stripe customer create
     * @param array $data
     * @return Response
     */
    protected function handleCustomerCreated($data)
    {
        $data = $data['data']['object'];
        $stripe_id['stripe_id'] = $data['id'];
        $email = $data['email'];
        $user = User::findByEmail($email);
        try {
            if ($user->load($stripe_id, '')) {
                $user->update();
            }
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
        }

    }

    /**
     * Handle Stripe customer source Created
     * @param array $data
     * @return Response
     */

    protected function handleCustomerSourceCreated($data)
    {
        $data = $data['data']['object'];
        $stripe_id = $data['customer'];
        $newData = [
            'card_brand' => $data['brand'],
            'card_last_four' => $data['last4']
        ];
        $user = User::find()->where(['stripe_id' => $stripe_id])->one();
        try {
            if ($user->load($newData, '')) {
                $user->update();
            }
        } catch (\yii\base\Exception $exception) {
            Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
        }
    }

    /**
     * Handle Stripe invoice payment failed
     * @param array $data
     * @return Response
     */
    protected function handleInvoicePaymentFailed($data)
    {
        $data = $data['data']['object'];
        $sub_id = $data['subscription'];
        $model = SubscriptionModel::find()->where(["stripe_id" => $sub_id])->one();

        if ($model) {
            try {
                $model->markAsCancelled();
                $model->update();
            } catch (\yii\base\Exception $exception) {
                Yii::error($exception->getMessage() . " " . $exception->getTraceAsString());
            }
        }
    }

    /**
     * Handle Stripe customer subscription deleted
     * @param array $payload
     * @return Response
     */
    protected function handleCustomerSubscriptionDeleted($payload)
    {
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);
        if ($user) {
            $subscriptions = $user->getSubscriptions()->all();
            /* @var $subscription SubscriptionModel */
            foreach ($subscriptions as $subscription) {
                if ($subscription->stripe_id === $payload['data']['object']['id']) {
                    $subscription->markAsCancelled();
                }
            }
        }

        return new Response([
            'statusCode' => 200,
            'statusText' => 'Webhook Handled',
        ]);
    }

    /**
     * Get the billable entity instance by Stripe ID.
     * @param string $stripeId
     * @return null|static
     */
    protected function getUserByStripeId($stripeId)
    {
        $model = Yii::$app->user->identityClass;
        return $model::findOne(['stripe_id' => $stripeId]);
    }

    /**
     * Verify with Stripe that the event is genuine.
     * @param string $id
     * @return bool
     */
    protected function eventExistsOnStripe($id)
    {
        try {
            return !is_null(StripeEvent::retrieve($id, Yii::$app->params['stripe']['apiKey']));
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Handle calls to missing methods on the controller.
     * @return mixed
     */
    public function missingMethod()
    {
        return new Response([
            'statusCode' => 200,
        ]);
    }
}
